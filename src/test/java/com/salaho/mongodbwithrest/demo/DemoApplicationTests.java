package com.salaho.mongodbwithrest.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	
	int actual = 1;
	int expected = 1;

	@Test
	public void contextLoads() {
		assertEquals(actual, expected);
	}
	@Test
	public void anotherTest() {
		assertNotEquals(actual, 5);
	}

}
